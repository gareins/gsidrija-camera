# Requirements

install these:

* libasound2-dev
* libv4l-dev
* libx264-dev
* git

Clone FFmpeg, checkout latest 3.4 branch, most likely do:

```git checkout n3.4.8```

Then run configuration script with these parameters:

```./configure --enable-libv4l2 --enable-libx264 --enable-gpl```

Then build with `make -j4` and `sudo make install`


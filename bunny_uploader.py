import ftplib
import os
import io
import sys
import time
import datetime


URL = "storage.bunnycdn.com"
USERNAME = "mm-gsidrija"
FOLDER = "recordings-idrija"
PASSWORD = os.environ["APIKEY"]
PIPE_FILE = sys.argv[1]
CHUNK_SIZE = 65534

connection = ftplib.FTP(host=URL, user=USERNAME, passwd=PASSWORD)
connection.cwd("/{}/{}".format(USERNAME, FOLDER))

fileout = datetime.datetime.now().isoformat().split(".")[0] + ".m2t" 
print("Python started: " + fileout)

with open(PIPE_FILE, "rb") as fp:
    while True:
        data_in = fp.read(CHUNK_SIZE)
        if len(data_in) == 0:
            break

        to_upload = io.BytesIO(data_in)
        connection.storbinary("APPE " + fileout, to_upload)
        print("Python: data uploaded")

print("Python stopped: " + fileout)



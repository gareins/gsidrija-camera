#!/bin/bash

LOGFILE=$HOME/simplestream.log
APIKEY_FILE="APIKEY"
FF_PIPE=/tmp/bunny_pipe
PY_BUNNY="bunny_uploader.py"

echo "$(date) Start" > $LOGFILE

function v4l2_settings {
    v4l2-ctl --set-ctrl h264_i_frame_period=30
    v4l2-ctl --set-ctrl h264_profile=Baseline
    echo "$(date) V4L2 CTL complete" >> $LOGFILE
}

function upload_to_bunny {
    echo "$(date) Create FIFO" >> $LOGFILE
    rm $FF_PIPE
    mkfifo $FF_PIPE

    sleep 10
    echo "$(date) Start bunny" >> $LOGFILE
    APIKEY=$(cat $APIKEY_FILE) python3 $PY_BUNNY $FF_PIPE & >> $LOGFILE

    ffmpeg -y -i rtsp://10.1.31.158:1910/video0/unicast -c:v copy -c:a mp2 -f mpegts $FF_PIPE
}

v4l2_settings
upload_to_bunny &

# v4l2rtspserver -v -W 1280 -H 720 -F 30 -P 1910 -c 1 /dev/video0,hw:1 1>>$LOGFILE 2>&1

echo "$(date) Start rtspserver" >> $LOGFILE
v4l2rtspserver -W 1280 -H 720 -F 30 -P 1910 -c 1 /dev/video0,hw:1 1>>$LOGFILE 2>&1

echo "$(date) Closed" >> $LOGFILE

